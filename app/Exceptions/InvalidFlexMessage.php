<?php

namespace App\Exceptions;

class InvalidFlexMessage extends \Exception
{
    protected $message = 'Invalid flex message: The message is not an emergency';
}
