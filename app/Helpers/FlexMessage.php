<?php

namespace App\Helpers;

use App\Exceptions\InvalidFlexMessage;
use App\Models\Emergency;
use Illuminate\Support\Str;

class FlexMessage
{
    private string $rawMessage;

    public function __construct(string $message)
    {
        $this->rawMessage = $message;
    }

    public function toArray(): array
    {
        try {
            return [
                'raw_message' => $this->rawMessage,
                'message' => $this->getMessage(),
                'priority' => $this->getPriority(),
                'service' => $this->getEmergencyService(),
                'cap_codes' => $this->getCapCodes()
            ];
        } catch (\Exception $e) {
            throw new InvalidFlexMessage($e->getMessage());
        }
    }

    public function getPriority(): int | null
    {
        $high = ['P1', 'P 1', 'PRIO 1', 'A1', 'A 1'];
        $medium = ['P2', 'P 2', 'PRIO 2', 'A2', 'A 2'];
        $low = ['P3', 'P 3', 'PRIO 3', 'A3', 'A 3', 'B1', 'B 1', 'B 2', 'B2', 'B3', 'B '];

        if (Str::contains($this->rawMessage, $high)) return Emergency::PRIORITY_HIGH;
        if (Str::contains($this->rawMessage, $medium)) return Emergency::PRIORITY_MEDIUM;
        if (Str::contains($this->rawMessage, $low)) return Emergency::PRIORITY_LOW;

        return Emergency::PRIORITY_UNKNOWN;
    }

    protected function getEmergencyService(): string|null
    {
        if ($this->isMedicalService()) {
            return 'AMBULANCE';
        } else if ($this->isFireDepartment()) {
            return 'FIRE';
        }

        throw new \Exception('Responding service is unknown');
    }

    public function getMessage(): string
    {
        return explode('ALN|', $this->rawMessage)[1];
    }

    protected function getCapCodes(): array
    {
        // retrieve the part that contain cap codes
        preg_match("/\|[0-9 ]+\|/", $this->rawMessage, $part);
        $part = $part[0];

        // if there aren't any, return
        if (!$part) return [];

        $part = str_replace("|", "", $part);

        // remove the excess 00
        return array_map(function ($part) {
            return ltrim($part, "00");
        }, explode(" ", $part));
    }

    protected function isFireDepartment(): bool
    {
        $possibilities = ['P1', 'P2', 'P3', 'P 1', 'P 2', 'P3', 'PRIO 1', 'PRIO 2', 'PRIO 3'];
//        $confirmations = ['PB', 'PR 1', 'PR 2', 'HV1', 'HV2', 'HV3', 'HV5', 'BR 1', 'BR 2', 'BR 3', ' BR ', 'DIER TE WATER', 'REANIMATIE', ' OMS '];

        if (!Str::contains($this->rawMessage, $possibilities)) return false;

        return true;
//        return Str::contains(strtoupper($this->rawMessage), $confirmations);
    }

    protected function isMedicalService(): bool
    {
        return preg_match("/\|(A[1-3])|\|(B[1-3])/", $this->rawMessage) != null;
    }
}
