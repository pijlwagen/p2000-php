<?php

namespace App\Http\Controllers\Emergency;

use App\Exceptions\InvalidFlexMessage;
use App\Helpers\FlexMessage;
use App\Http\Controllers\Controller;
use App\Http\Requests\Emergency\EmergencyRequest;
use App\Jobs\ProcessEmergency;
use App\Models\Emergency;
use App\Models\RespondingUnit;
use Illuminate\Support\Facades\DB;

class EmergencyController extends Controller
{
    public function create(EmergencyRequest $request): Emergency | \Illuminate\Http\JsonResponse
    {
        $message = new FlexMessage($request->validated()['message']);

        try {
            $data = $message->toArray();
        } catch (InvalidFlexMessage $e) {
            return response()
                ->json(['code' => 400, 'message' => $e->getMessage()])
                ->setStatusCode(400);
        }

        $emergency = Emergency::create($data);

        $this->createCapCodes($data['cap_codes']);
        $this->linkRespondingUnits($emergency, $data['cap_codes']);

        ProcessEmergency::dispatch($emergency);

        return response()
            ->api($emergency);
    }

    public function index()
    {
        return Emergency::all();
    }

    private function createCapCodes(array $array): void
    {
        DB::table('units')
            ->insertOrIgnore(array_map(function ($code) {
                return [
                    'id' => $code
                ];
            }, $array));
    }

    private function linkRespondingUnits(Emergency $emergency, array $capCodes): void
    {
        RespondingUnit::insert(
            array_map(function ($code) use ($emergency) {
                return [
                    'unit_id' => $code,
                    'emergency_id' => $emergency->id
                ];
            }, $capCodes)
        );
    }
}
