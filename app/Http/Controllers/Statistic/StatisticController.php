<?php

namespace App\Http\Controllers\Statistic;

use App\Http\Controllers\Controller;
use App\Models\Emergency;
use Carbon\Carbon;
use Illuminate\Support\Collection;

class StatisticController extends Controller
{
    public function pastTwentyFourHours()
    {
        $frames = Emergency::query()
            ->where('created_at', '>=', Carbon::now()->subDay()->toDateTimeString())
            ->selectRaw('count(*) as `calls`, `service`, HOUR(`created_at`) as hour')
            ->groupByRaw('HOUR(`created_at`) ASC, `service`')
            ->get();

        $total = [
            Emergency::SERVICE_AMBULANCE => 0,
            Emergency::SERVICE_FIRE_DEPT => 0,
        ];

        foreach ($frames as $frame) {
            if (array_key_exists($frame->service, $total)) {
                $total[$frame->service] += $frame->calls;
            }
        }

        return response()
            ->api([
                'total' => $total,
                'graph' => $frames
            ]);
    }

    public function pastSevenDays()
    {
        $frames = Emergency::query()
            ->where('created_at', '>=', Carbon::now()->subDays(7)->toDateTimeString())
            ->selectRaw('count(*) as `calls`, `service`, HOUR(`created_at`) as hour')
            ->groupByRaw('HOUR(`created_at`) ASC, `service`')
            ->get();

        $total = [
            Emergency::SERVICE_AMBULANCE => 0,
            Emergency::SERVICE_FIRE_DEPT => 0,
        ];

        foreach ($frames as $frame) {
            if (array_key_exists($frame->service, $total)) {
                $total[$frame->service] += $frame->calls;
            }
        }

        return response()
            ->api([
                'total' => $total,
                'graph' => $frames
            ]);

        return response()
            ->api($data);
    }
}
