<?php

namespace App\Jobs;

use App\Events\NewEmergencyNotification;
use App\Models\Emergency;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class ProcessEmergency implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $emergency;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Emergency $emergency)
    {
        $this->emergency = $emergency;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        NewEmergencyNotification::broadcast($this->emergency);
    }
}
