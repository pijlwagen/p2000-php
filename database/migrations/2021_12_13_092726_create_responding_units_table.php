<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRespondingUnitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('responding_units', function (Blueprint $table) {
            $table->foreignId('emergency_id')
                ->references('id')
                ->on('emergencies')
                ->cascadeOnDelete();

            $table->foreignId('unit_id')
                ->references('id')
                ->on('units')
                ->cascadeOnDelete();

            $table->unique(['emergency_id', 'unit_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('responding_units');
    }
}
