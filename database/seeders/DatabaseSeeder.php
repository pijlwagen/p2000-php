<?php

namespace Database\Seeders;

use App\Models\Unit;
use Illuminate\Database\Seeder;
use Illuminate\Support\Collection;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $capcodes = json_decode(
            file_get_contents(base_path('database\seeders\data\capcodes.json'))
        );

        $collection = new Collection();

        foreach ($capcodes as $capcode) {
            if ($collection->has($capcode->id)) continue;

            $collection->put($capcode->id, [
                'id' => $capcode->id,
                'name' => $capcode->name,
                'region' => $capcode->region,
                'district' => $capcode->district
            ]);
        }

        Unit::insert($collection->values()->toArray());
    }
}
