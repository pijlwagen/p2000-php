<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Emergency\EmergencyController;
use App\Http\Controllers\Statistic\StatisticController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
    'prefix' => '/emergency'
], function () {
    Route::post('/', [EmergencyController::class, 'create']);
    Route::get('/', [EmergencyController::class, 'index']);
});

Route::group([
    'prefix' => '/statistics'
], function () {
    Route::get('/24-hours', [StatisticController::class, 'pastTwentyFourHours']);
    Route::get('/7-days', [StatisticController::class, 'pastSevenDays']);
});
